'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

const Route = use('Route')


Route.get('/', 'TicketController.index')
Route.get('/tickets/edit/:id', 'TicketController.edit')
Route.post('/tickets/edit/:id', 'TicketController.update').validator('Ticket')

Route.get('new_ticket', 'TicketController.create')
Route.post('new_ticket', 'TicketController.store').validator('Ticket')

Route.on('/auth/register').render('auth.sign-up')
Route.on('/auth/login').render('auth.sign-in')

Route.get('/logout', async ({auth, response}) => {
    await auth.logout()
    return response.redirect('/')
})

Route.post('/auth/register', 'UserController.store').validator('UserSignUp')
Route.post('/auth/login', 'UserController.login').validator('SignInUser')

Route.on('/chat').render('chat.index')

