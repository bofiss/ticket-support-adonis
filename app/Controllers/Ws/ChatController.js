'use strict'

class ChatController {
  constructor ({ socket, request }) {
    this.socket = socket
    this.request = request
  }


  // listion event `ready`
  onReady () {
    console.log('ready');
    this.socket.toMe().emit('my:id', this.socket.socket.id)
  }

  onMessage(message) {
    this.socket.toEveryone.emit('message', message)

  }

  joinRoom(ContextWs, payload) {

  }

  leaveRoom(ContextWs, payload) {

  }
}

module.exports = ChatController
