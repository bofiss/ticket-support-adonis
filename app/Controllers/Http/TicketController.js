'use strict'

const Category = use ('App/Models/Category')
const Ticket = use ('App/Models/Ticket')
const User = use ('App/Models/User')

class TicketController {

    async index({ view, response, auth }) {

        try {
            const user = await auth.getUser()
            await user.load('tickets')
            return view.render('tickets.index', {user: user.toJSON()})
          } catch (error) {

            return view.render('tickets.index')
        }

    }
 
    async create({ view}) {
        const categories = await Category.all()
        return view.render('tickets.create', {categories: categories.toJSON()})
    }


    async store({ request, response, session, auth }) {

        const category = await Category.findBy('name', request.input('category'))
        const user = await auth.getUser()

        const ticket=  await Ticket.create({
            category_id: category.toJSON().id,
            user_id: user.id,
            title: request.input('title'),
            priority: request.input('priority'),
            message: request.input('message'),
            status: request.input('status')
        })

        session.flash({notification: 'Ticket added!'})
     
        return response.redirect('/')
    }

    async edit({ view, request,  session, params , auth}) {

   console.log(params.id)
        let categ, prior
        const ticket = await Ticket.find(params.id)  

        let categories = await Category.all()
        categories = categories.toJSON()
        categories.filter(cat => cat.id === ticket.category_id).map(cat => categ = cat)

        const tickets = await Ticket.all()
        const priorities = Array.from(new Set(tickets.toJSON().map(p => p.priority)))
        priorities.filter(p => p === ticket.priority).map(p => prior = p)


        if(ticket){
            return view.render('tickets.edit', 
            {
                categories: categories.filter(cat => cat.id !== ticket.category_id),
                ticket: ticket,
                category: categ,
                priority: prior,
                priorities: priorities.filter(p => p !== ticket.priority)
            })
        }
    
        session.flash({notification: 'Ticket not found!'})
        return response.redirect('back')
    }

    async update({ request, response, session, params, auth }) {



        
        const ticket = await Ticket.find(params.id)
        const category = await Category.findBy('name', request.input('category'))
        const user = await auth.getUser()

        if (ticket) {
           ticket.category_id =  category.toJSON().id
           ticket.category = request.input('title')
           ticket.status = request.input('status')
           ticket.title = request.input('title')
           ticket.message = request.input('message')
           
            await ticket.save()
            session.flash({notification: 'Ticket updated successffuly'})
            return response.redirect('/')
        }

        
        session.flash({notification: 'Ticket was not found !'})
     
        return response.redirect('back')
    }
    

    

 
}

module.exports = TicketController



/*class RedirectIfAuthenticated {
  async handle ({ request, response, session, auth }, next) {
    if (auth.user) {
      session.flash({notification: 'You are already logged in!'})
      return response.redirect('/')
    }
    await next()
  }
}

module.exports = RedirectIfAuthenticated*/
