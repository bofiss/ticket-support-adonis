'use strict'

const User = use('App/Models/User')

class UserController {

    async store({ request, response, auth }) {

       const user =  await User.create(request.only([ 'name', 'username', 'email', 'password' ]))
    
       await auth.login(user)

       return response.redirect('/')
    }

    async login({ request, response, auth, session}) {

        const { email, password } = request.all()

        try {
            await auth.attempt(email, password)

            session.flash({notification: 'Login successfull!'})
            return response.redirect('/')

        } catch (error) {
       
            session.flash({error: 'These credentials doesn\'t match our records' })

            return response.redirect('back')
        }
       
        
       
    }

    async logout({ request, response, auth}) {

         await auth.logout()
        
        return response.redirect('/')
    }
}


module.exports = UserController
