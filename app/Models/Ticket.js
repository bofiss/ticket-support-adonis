'use strict'

const Model = use('Model')

class Ticket extends Model {

    user () {
        return this.belongsTo('App/Models/User')
    }

    comments () {
        return this.hasMany('App/Models/Comment')
    }

    category () {
        return this.belongsTo('App/Models/Category')
    }
}

module.exports = Ticket
