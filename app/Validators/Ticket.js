'use strict'

class Ticket {
  get rules () {
    return {
      'title': 'required',
      'category': 'required',
      'priority': 'required',
      'message': 'required'
    }
  }

  get messages () {
    return {
      'required': 'The {{ field }} is required'
    }
  }

  async fails (errorMessages) {
    this.ctx.session.withErrors(errorMessages).flashAll();
    this.ctx.response.redirect('back')
  }

}

module.exports = Ticket
