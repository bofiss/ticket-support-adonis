'use strict'

class UserSignUp {

  get rules () {
    return {
      'name': 'required|unique:users',
      'username': 'required|unique:users',
      'email': 'required|email|unique:users',
      'password': 'required',
      'password-confirmation': 'required'
    }
  }

  get messages () {
    return {
      'required': 'The {{ field }} is required',
      'unique'  :  'The {{ field }} is already taken'
    }
  }

  async fails (errorMessages) {
    this.ctx.session.withErrors(errorMessages).flashAll();
    this.ctx.response.redirect('back')
  }

}

module.exports = UserSignUp
